test:
    cargo build
    cargo check
    cargo clippy
    cargo fmt

    codespell --skip po,target -L crate
