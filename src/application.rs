/* application.rs
 *
 * Copyright 2023 Epistula Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::subclass::prelude::*;
use gettextrs::gettext;
use gtk::prelude::*;
use gtk::{gio, glib};

use crate::EpistulaWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct EpistulaApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for EpistulaApplication {
        const NAME: &'static str = "EpistulaApplication";
        type Type = super::EpistulaApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for EpistulaApplication {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);

            obj.set_accels_for_action("app.new-window", &["<primary>n"]);
            obj.set_accels_for_action("app.paste", &["<primary>v"]);
            obj.set_accels_for_action("app.open-file", &["<primary>o"]);
        }
    }

    impl ApplicationImpl for EpistulaApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self) {
            let application = self.obj();
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                EpistulaWindow::new(&*application).upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }
    }

    impl GtkApplicationImpl for EpistulaApplication {}
    impl AdwApplicationImpl for EpistulaApplication {}
}

glib::wrapper! {
    pub struct EpistulaApplication(ObjectSubclass<imp::EpistulaApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl EpistulaApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder()
            .property("application-id", application_id)
            .property("flags", flags)
            .build()
    }

    fn setup_gactions(&self) {
        let new_window_action = gio::ActionEntry::builder("new-window")
            .activate(move |app: &Self, _, _| app.new_window())
            .build();
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();

        let paste_action = gio::ActionEntry::builder("paste")
            .activate(move |app: &Self, _, _| app.on_paste())
            .build();
        let open_file_action = gio::ActionEntry::builder("open-file")
            .activate(move |app: &Self, _, _| app.on_open_file())
            .build();

        self.add_action_entries([
            new_window_action,
            quit_action,
            about_action,
            paste_action,
            open_file_action,
        ]);
    }

    fn new_window(&self) {
        EpistulaWindow::new(self)
            .upcast::<adw::ApplicationWindow>()
            .present();
    }

    fn show_about(&self) {
        let about =
            adw::AboutWindow::from_appdata("/io/gitlab/gregorni/Epistula/metainfo.xml", None); //"1.0");
        about.set_transient_for(Some(&self.active_window().unwrap()));
        about.set_developers(&["Gregor Niehl"]);
        about.set_copyright(&gettext("© 2023 Epistula Contributors"));
        about.add_acknowledgement_section(
            Some(&gettext("Code Borrowed From")),
            &["Switcheroo https://gitlab.com/adhami3310/Switcheroo"],
        );

        about.add_legal_section(
            "Ares",
            Some("© 2021 Bee @bee-san on GitHub"),
            gtk::License::MitX11,
            None,
        );

        about.present();
    }

    fn on_paste(&self) {
        self.active_window()
            .unwrap()
            .downcast::<EpistulaWindow>()
            .unwrap()
            .paste_from_clipboard();
    }

    fn on_open_file(&self) {
        self.active_window()
            .unwrap()
            .downcast::<EpistulaWindow>()
            .unwrap()
            .open_file();
    }
}
