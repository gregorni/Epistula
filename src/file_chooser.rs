/* file_chooser.rs
 *
 * Copyright 2023 Epistula Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gettextrs::gettext;
use glib::clone;
use gtk::{gio, glib};

use crate::window::EpistulaWindow;
use adw::prelude::FileExt;

pub struct FileChooser;

impl FileChooser {
    pub fn open_and_read_file<T>(parent: &EpistulaWindow, callback_success: T)
    where
        T: Fn(&EpistulaWindow, gio::File) + 'static,
    {
        glib::MainContext::default().spawn_local(clone!(@strong parent => async move {
            let text_filter = gtk::FileFilter::new();
            text_filter.add_mime_type("text/plain");
            text_filter.set_name(Some(&gettext("Text Files")));

            let response = gtk::FileDialog::builder()
                .accept_label(gettext("Open"))
                .title(gettext("Select File"))
                .modal(true)
                .default_filter(&text_filter)
                .build()
                .open_future(Some(&parent))
                .await;

            if let Ok(file) = response {
                callback_success(&parent, file);
            }
        }));
    }

    pub fn write_and_save_file<T>(parent: &EpistulaWindow, callback_success: T)
    where
        T: Fn(&EpistulaWindow, String) + 'static,
    {
        glib::MainContext::default().spawn_local(clone!(@strong parent => async move {
            let text_filter = gtk::FileFilter::new();
            text_filter.add_mime_type("text/plain");

            let response = gtk::FileDialog::builder()
                .accept_label(gettext("Save"))
                .title(gettext("Save File"))
                .initial_name("output.txt")
                .modal(true)
                .default_filter(&text_filter)
                .build()
                .save_future(Some(&parent))
                .await;

            if let Ok(file) = response {
                let file_path = file.path().unwrap();

                callback_success(&parent, file_path.to_str().unwrap().to_owned());
            }
        }));
    }
}
