/* window.rs
 *
 * Copyright 2023 Epistula Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use crate::config;
use adw::subclass::prelude::*;
use gettextrs::gettext;
use glib::clone;
use gtk::prelude::*;
use gtk::{gdk, gio, glib};

use crate::file_chooser::FileChooser;
use std::fs;

mod imp {
    use super::*;

    #[derive(Debug, gtk::CompositeTemplate, derivative::Derivative)]
    #[derivative(Default)]
    #[template(resource = "/io/gitlab/gregorni/Epistula/gtk/window.ui")]
    pub struct EpistulaWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub drag_overlay: TemplateChild<gtk::Overlay>,
        #[template_child]
        pub drag_revealer: TemplateChild<gtk::Revealer>,
        #[template_child]
        pub toast_overlay: TemplateChild<adw::ToastOverlay>,
        #[template_child]
        pub main_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub paste_btn: TemplateChild<gtk::Button>,
        #[template_child]
        pub open_file_btn: TemplateChild<gtk::Button>,
        #[template_child]
        pub spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        pub input_text_view: TemplateChild<gtk::TextView>,
        #[template_child]
        pub output_text_view: TemplateChild<gtk::TextView>,
        #[derivative(Default(value = "gio::Settings::new(config::APP_ID)"))]
        pub settings: gio::Settings,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EpistulaWindow {
        const NAME: &'static str = "EpistulaWindow";
        type Type = super::EpistulaWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EpistulaWindow {
        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            self.settings
                .bind("window-height", obj.as_ref(), "default-height")
                .build();
            self.settings
                .bind("window-width", obj.as_ref(), "default-width")
                .build();
            self.settings
                .bind("window-is-maximized", obj.as_ref(), "maximized")
                .build();

            self.paste_btn
                .connect_clicked(clone!(@weak obj => move |_button| {
                    obj.paste_from_clipboard();
                }));

            self.open_file_btn
                .connect_clicked(clone!(@weak obj => move |_button| {
                    obj.open_file();
                }));
        }
    }
    impl WidgetImpl for EpistulaWindow {}
    impl WindowImpl for EpistulaWindow {}
    impl ApplicationWindowImpl for EpistulaWindow {}
    impl AdwApplicationWindowImpl for EpistulaWindow {}
}

glib::wrapper! {
    pub struct EpistulaWindow(ObjectSubclass<imp::EpistulaWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl EpistulaWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        let win = glib::Object::builder::<EpistulaWindow>()
            .property("application", application)
            .build();

        win.setup_drag_n_drop();

        win
    }

    fn setup_drag_n_drop(&self) {
        let drag_action = gdk::DragAction::COPY;
        let drop_target = gtk::DropTarget::builder()
            .actions(drag_action)
            .formats(&gdk::ContentFormats::for_type(gio::File::static_type()))
            .build();

        let imp = self.imp();

        drop_target.connect_enter(
            clone!(@weak imp => @default-return drag_action, move |_,_,_| {
                imp.drag_revealer.set_reveal_child(true);
                drag_action
            }),
        );

        drop_target.connect_leave(clone!(@weak imp => move |_| {
            imp.drag_revealer.set_reveal_child(false);
        }));

        drop_target.connect_drop(
            clone!(@weak self as this => @default-return false, move |_, value, _, _| {
                if let Ok(file) = value.get::<gio::File>() {
                    this.crack_file_contents(file);

                    return true;
                }

                false
            }),
        );

        self.add_controller(drop_target);
    }

    fn crack_file_contents(&self, file: gio::File) {
        let file_contents = fs::read_to_string(file.path().unwrap().to_str().unwrap())
            .expect("Could not read selected file");

        self.try_cracking(&file_contents);
    }

    pub fn paste_from_clipboard(&self) {
        glib::MainContext::default().spawn_local(clone!(@weak self as this => async move {
            let clipboard_request = gdk::Display::default()
                .expect("No display found.")
                .primary_clipboard()
                .read_text_future()
                .await;

            dbg!(&clipboard_request);

            if let Ok(Some(pasted_text)) = clipboard_request {
                this.try_cracking(&pasted_text);
            } else {
                this.imp().toast_overlay.add_toast(adw::Toast::new(&gettext("Clipboard Empty")));
            }
        }));
    }

    pub fn open_file(&self) {
        FileChooser::open_and_read_file(self, |this, file_contents| {
            this.crack_file_contents(file_contents);
        });
    }

    fn try_cracking(&self, input_text: &str) {
        let imp = self.imp();

        imp.spinner.set_spinning(true);
        imp.main_stack.set_visible_child_name("loading");

        let input_text = input_text.to_string();
        // Create channel that can hold at most 1 message at a time
        let (sender, receiver) = async_channel::bounded(1);
        let sender = sender.clone();

        let timeout_in_seconds = 30;

        // The long running operation runs now in a separate thread
        gio::spawn_blocking(move || {
            // Deactivate the button until the operation is done
            sender
                .send_blocking(None)
                .expect("The async channel isn't open at the beginning");

            let mut ares_config = ares::config::Config::default();
            ares_config.timeout = timeout_in_seconds;
            let decode_result = ares::perform_cracking(&input_text, ares_config);

            // Activate the button again
            sender
                .send_blocking(Some(decode_result))
                .expect("The async channel isn't open");
        });

        // The main loop executes the asynchronous block
        glib::spawn_future_local(clone!(@weak imp => async move {
            while let Ok(decode_result) = receiver.recv().await {
                if let Some(decode_result) = decode_result {
                    imp.spinner.set_spinning(false);
                    if let Some(decode_result) = &decode_result {
                        dbg!(decode_result);

                        let input_buffer = imp.input_text_view.buffer();
                        let output_buffer = imp.output_text_view.buffer();

                        input_buffer.set_text(&decode_result.path[0].encrypted_text);
                        output_buffer.set_text(&decode_result.text[0]);

                        imp.main_stack.set_visible_child_name("solved");
                    } else {
                        imp.main_stack.set_visible_child_name("welcome");
                        imp.toast_overlay.add_toast(adw::Toast::new(&gettext("Couldn't decode input")));
                    }
                }
            }
        }));
    }
}
